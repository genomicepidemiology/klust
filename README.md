# ?? Documentation #

?? combines KMA dist and ccphylo to allow the user to calculate a distance/similarity matrix of DNA sequences and cluster them with different methods

## Content of the repository ##
1. ??.py - Python code that calls kma and ccphylo
2. README

## Installation ##
The program requires the software KMA (https://bitbucket.org/genomicepidemiology/kma.git)(in particular, the Index and Dist methods) and Ccphylo (https://bitbucket.org/genomicepidemiology/ccphylo.git). If they are not in your computer, follow the instructions described in their repositories.

## Usage ##

You can run ?? command line using Python3.
```bash
usage: kma_distance.py [-h]
                       [-ifa INPUTFASTA | -idx INPUTINDEXED | -iphy INPUTPHYLIP]
                       -o OUT_PATH [-kp KMA_PATH] [-ccp CCPHYLO_PATH]
                       [-tmp TMP_PATH] [-f] [-nc] [-d DIST_METHOD]
                       [-mt {upgma,cf,nj,mn,ff}] [-m] [-t THREADS]

optional arguments:
  -h, --help            show this help message and exit
  -ifa INPUTFASTA, --inputfasta INPUTFASTA
                        Input fasta file
  -idx INPUTINDEXED, --inputindexed INPUTINDEXED
                        Input KMA indexed files
  -iphy INPUTPHYLIP, --inputphylip INPUTPHYLIP
                        Input phylip file (end with .phy)
  -o OUT_PATH, --outputPath OUT_PATH
                        Path to output folder
  -kp KMA_PATH, --kmaPath KMA_PATH
                        Path to KMA
  -ccp CCPHYLO_PATH, --ccphyloPath CCPHYLO_PATH
                        Path to ccphylo
  -tmp TMP_PATH, --tmpPath TMP_PATH
                        Path to temporary folder
  -f, --flagOut         Include distance method(s) in phylip file
  -nc, --no_cluster     Create Phylip file and do not cluster data
  -d DIST_METHOD, --distMethod DIST_METHOD
                        Select the type of distance/similarity method(s).
                        Types: {1: k-mer hamming distance, 2: shared k-mers,
                        4: k-mer query coverage, 8: k-mer template coverage,
                        16: k-mer avg. coverage, 32: k-mer inv. avg. coverage,
                        64: Jaccard distance, 128: Jaccard similarity, 256:
                        Cosine distance, 512: Cosine similarity, 1024:
                        Szymkiewicz–Simpson similarity, 2048:
                        Szymkiewicz–Simpson dissimilarity, 4096: Chi-square
                        distance}. If you introduce more than a method, use a
                        comma for separating them.
  -mt {upgma,cf,nj,mn,ff}, --methodTree {upgma,cf,nj,mn,ff}
                        Tree construction method: {nj: Neighbour-Joining,
                        upgma: UPGMA, cf: K-means Closest First, ff: K-means
                        Furthest First, mn: Minimum Neighbours}
  -m, --matrixDisk      Allocate matrix in the disk
  -t THREADS, --threads THREADS
                        Number of threads used during the creation of the
                        distance/similarity matrix

```

## References ##
1. Clausen PTLC, Aarestrup FM, Lund O. Rapid and precise alignment of raw reads against redundant databases with KMA. BMC Bioinformatics 2018; 19:307.
2. Malte B. Hallgren, Søren Overballe-Petersen, Ole Lund, Henrik Hasman & Philip T.L.C. Clausen, "MINTyper: A method for generating phylogenetic distance matrices with long read sequencing data", bioRxiv 2020.

## License ##
Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
