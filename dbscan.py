#!/usr/bin/env python3
from __future__ import division
import os
import treeswift
from collections import deque
from tree import Tree
from dbscan_objects import DBSCANCluster, DBSCANOutlierPool, DBSCANLimbo
from dbscan_objects import DBSCANPoint
from argparse import ArgumentParser


class DBSCAN(Tree):

    def __init__(self, file, out_folder, write_clusters=False,
                 write_outliers=False):

        super().__init__(file)

        if write_clusters:
            self.out_clusters = out_folder
        else:
            self.out_clusters = None

        if write_outliers:
            self.out_outliers = out_folder
        else:
            self.out_outliers = None

        self.out_folder = os.path.abspath(out_folder)

        self._initiate_resume()

    def _initiate_resume(self):
        if not os.path.isdir(self.out_folder):
            raise OSError("%s is nto a folder." % self.out_folder)
        self.out_file = ((self.out_folder
                            + "/%s_cluster_resume.tsv" % str(self.file_name)))
        with open(self.out_file, "w") as outfile:
            outfile.write(("Clustering resume of the file %s\n"
                            % str(self.file_name)))
            outfile.write("Cluster\tSequences\n")

    def write_cluster(self, cluster, outlier=False):
        with open(self.out_file, "a") as outfile:
            if outlier:
                outfile.write("Outliers\t%s\n" % (cluster))
            else:
                outfile.write("ClusterN %s\t%s\n" % (cluster.CLUSTER_NUMBER,
                                                     cluster))
        cluster.close()

    def cluster(self, max_dist, min_neighb):

        leaves_iterator = self.tree.traverse_leaves()
        cluster = None
        pre_clust = DBSCANLimbo()
        outliers_pool = DBSCANOutlierPool()

        while True:
            try:
                leaf = next(leaves_iterator)
            except StopIteration:
                if cluster is not None:
                    self.write_cluster(cluster)
                if pre_clust:
                    for outlier in pre_clust:
                        outliers_pool.append(outlier)
                self.write_cluster(outliers_pool, outlier=True)
                break
            dbpoint = DBSCANPoint(leaf)
            if cluster is not None:

                last_core = cluster.last_neighbour
                last_core_dist = self.tree.distance_between(
                                    last_core.point, dbpoint.point)
                if last_core_dist <= max_dist:
                    cluster.append_corepoint(dbpoint)
                else:
                    first_core = cluster.core_points[0]
                    first_core_dist = self.tree.distance_between(
                                        first_core.point, dbpoint.point)
                    if first_core_dist < max_dist:
                        cluster.append_borderpoint(dbpoint)
                    else:
                        dbpoint.type = "outlier"
                        pre_clust.append(dbpoint)
                        self.write_cluster(cluster)
                        cluster = None
            else:
                # If in the limbo + new point there is enough points for making
                # a cluster
                if pre_clust.length >= min_neighb + 1:
                    # Check if there is enough close points to make a cluster
                    count = 0
                    pre_core = deque()
                    pre_border = deque()
                    for limbo_point in pre_clust:
                        pre_core_dist = self.tree.distance_between(
                                            dbpoint.point, limbo_point.point)
                        if pre_core_dist <= max_dist:
                            limbo_point.type = "core"
                            pre_core.append(limbo_point)
                            count += 1
                        else:
                            limbo_point.type = "border"
                            pre_border.append(limbo_point)
                    # Add actual point
                    dbpoint.type = "core"
                    pre_core.appendleft(dbpoint)
                    count += 1
                    # If there is enough core points to make a cluster
                    if count >= min_neighb:
                        # Declare Cluster with the pre_core
                        cluster = DBSCANCluster(out_folder=self.out_clusters,
                                                core=pre_core)
                        # Iterate over the border candidates
                        for new_border in pre_border:
                            # It is expected that the last core will be closer
                            # to the border points
                            reverse_core = cluster.get_lastcorepoint()
                            pre_border_dist = self.tree.distance_between(
                                                    new_border.point,
                                                    reverse_core.point)
                            # It is a border point
                            if pre_border_dist <= max_dist:
                                cluster.append_borderpoint(new_border)
                            # It is an outlier
                            else:
                                outliers_pool.append(new_border)
                        # Reset Limbo as cluster has been created
                        pre_clust.reset()
                    # If there is not enough core points to make a cluster
                    else:
                        pre_clust.append(dbpoint)
                # If in the limbo + new point there is not enough points for
                # making a cluster
                else:
                    pre_clust.append(dbpoint)

        DBSCANCluster.CLUSTER_NUMBER = 0

    @staticmethod
    def arguments():
        parser = ArgumentParser()
        parser.add_argument("-i", "--inputTree",
                            dest="inputree",
                            help="Path to tree",
                            required=True)
        parser.add_argument("-o", "--outFolder",
                            dest="outputfolder",
                            help="Path to outfolder",
                            required=True)
        parser.add_argument("-d", "--maxDist",
                            dest="maxDist", type=float,
                            help="Maximum distance to relate two points",
                            required=True)
        parser.add_argument("-n", "--minNeighbour",
                            dest="minNeighbour", type=int,
                            help="Minimum amount of points to make a cluster",
                            required=True)
        parser.add_argument("-wc", "--writeClusters",
                            dest="writeClusters",
                            help="Write a file for each cluster",
                            action="store_true")
        parser.add_argument("-wo", "--writeOutliers",
                            dest="writeOutliers",
                            help="Write a file for the outliers",
                            action="store_true")
        return parser.parse_args()


if __name__ == '__main__':
    args = DBSCAN.arguments()
    instance = DBSCAN(args.inputree, args.outputfolder, args.writeClusters,
                      args.writeOutliers)
    instance.cluster(max_dist=args.maxDist, min_neighb=args.minNeighbour)
