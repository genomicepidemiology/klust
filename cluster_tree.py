#!/usr/bin/env python3
from __future__ import division
import os
import treeswift
from collections import deque
from cluster import Cluster
from tree import Tree


class ClusterTree(Tree):

    def __init__(self, file, type_tree):

        super().__init__(file)
        options_tree = ["kmeans_closest", "kmeans_furthest",
                        "neighbour-joining", "upgma", "minimum_neighbors"]
        if type_tree not in options_tree:
            raise ValueError("Type tree has to be %s " % (", ".join(options_tree)))
        self.type_tree = type_tree

    def kmeans_cluster(self, min_dist, parent_node, out_folder, first):
        if first == "Closest" or first == "Furthest":
            raise TypeError("The parameter 'First' has to be 'Closest' or "
                            "'Furthest'")
        children_nodes = parent_node.children
        unclustered_children = deque()
        if parent_node.is_leaf():
            Cluster(parent_node, out_folder)
        else:
            if len(children_nodes) != 2:
                raise KeyError("More than two children ndoes when parent is "
                               "not root.")
            tmp_clust = []
            dist_children = 0
            for child_node in children_nodes:
                dist_child = self.tree.distance_between(parent_node,
                                                        child_node)
                print(dist_child)
                dist_children += dist_child
                if first == "Closest":
                    if dist_child <= min_dist:
                        tmp_clust.append(child_node)
                    else:
                        unclustered_children.append(child_node)
                else:
                    if dist_child >= min_dist:
                        tmp_clust.append(child_node)
                    else:
                        unclustered_children.append(child_node)
            if first == "Closest":
                if dist_children <= min_dist:
                    Cluster(parent_node, out_folder)
                elif tmp_clust:
                    for tmp in tmp_clust:
                        Cluster(tmp, out_folder)
            else:
                if dist_children >= min_dist:
                    Cluster(parent_node, out_folder)
                elif tmp_clust:
                    for tmp in tmp_clust:
                        Cluster(tmp, out_folder)
        return unclustered_children

    def preorder_clustering(self, min_dist, first, out_folder=None):
        '''Perform a preorder traversal starting at this ``Node`` object
        '''
        s = deque()
        s.append(self.root)
        while len(s) != 0:
            n = s.pop()
            unclustered_children = self.kmeans_cluster(min_dist=min_dist,
                                                       parent_node=n,
                                                       out_folder=out_folder,
                                                       first=first)
            if unclustered_children:
                s.extend(n.children)

    def length_ultrametrictree(self):
        length_tree = 0
        for node in self.tree.traverse_preorder():
            if node.is_leaf():
                length_tree = self.tree.distance_between(self.root, node)
                break
        return length_tree

    def f_ultrametric_cluster(self, min_dist, parent_node, out_folder):
        unclustered_children = deque()
        if parent_node.is_leaf():
            Cluster(parent_node, out_folder)
        else:
            node_root_dist = self.tree.distance_between(self.root, parent_node)
            if node_root_dist > min_dist:
                Cluster(parent_node, out_folder)
            else:
                unclustered_children.append(parent_node.children)
        return unclustered_children

    def f_ultrametric_clustering(self, min_dist, out_folder):
        """Perform a clustering of an ultrametric tree"""
        length_tree = self.length_ultrametrictree()
        min_dist = min_dist-length_tree
        if min_dist <= 0:
            Cluster(self.root)
        else:
            s = deque()
            s.append(self.root)
            while len(s) != 0:
                n = s.pop()
                unclustered_children = self.f_ultrametric_cluster(
                                       min_dist=min_dist,
                                       parent_node=n,
                                       out_folder=out_folder)
                if unclustered_children:
                    s.extend(n.children)

    def ultrametric_clustering(self, min_dist, node, out_folder):
        pass


    def postorder_clustering(self, min_dist, out_folder):
        """Perform a clustering of an ultrametric tree"""
        s1 = deque()
        s1.append(self.root)
        while len(s1) != 0:
            n = s1.pop()
            s1.extend(n.children)
        first_leaf = n
        s2 = deque()
        s2.append(first_leaf)
        while len(s2) != 0:
            node = s2.pop()
            ###ultrametric clustering
            parent = node.parent
            node_parent_dist = self.tree.distance_between(node, parent)
            if node_parent_dist*2 <= min_dist:
                childrens_parent = parent.children
                for child_node in childrens_parent:
                    Cluster(child_node)
            else:
                continue

    def __call__(self, min_dist, out_folder):
        if self.type_tree == "kmeans_closest":
            self.preorder_clustering(min_dist, first="Closest",
                                     out_folder=out_folder)
        elif self.type_tree == "kmeans_furthest":
            self.preorder_clustering(min_dist, first="Furthest",
                                     out_folder=out_folder)
        elif self.type_tree == "upgma":
            for i in self.postorder_clustering(min_dist, out_folder):
                print(i, "OUT")


if __name__ == "__main__":
    example = ClusterTree("/home/alfred/Projects/klust/out/disinfectant_upgma.tree", type_tree="upgma")
    example(min_dist=100., out_folder="/home/alfred/Projects/klust/out/clusters")
    example.tree.draw()
