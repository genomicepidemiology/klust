Application class test
=================


###Testing of clustering
```python3
>>> from dbscan import DBSCAN
>>> example = DBSCAN("/home/alfred/Projects/klust/out/fosfomycin.tree", "/home/alfred/Projects/klust/out")
>>> a = example.cluster(max_dist=100, min_neighb=2)
>>> example = DBSCAN("/home/alfred/Projects/klust/out/betalactam_upgma.tree", "/home/alfred/Projects/klust/out")
>>> a = example.cluster(max_dist=100, min_neighb=2)
