#!/usr/bin/env python3
from __future__ import division
import sys
import os
import subprocess
from argparse import ArgumentParser


class KMA_Distance():

    def __init__(self, input_file, out_folder, out_name=None, tmp_folder=None,
                 kma_folder_path=None, ccphylo_path=None):

        self.input_file = input_file
        if kma_folder_path is not None:
            self.kma_folder_path = kma_folder_path + "/"
        else:
            self.kma_folder_path = ""
        if ccphylo_path is not None:
            self.ccphylo_path = ccphylo_path + "/"
        else:
            self.ccphylo_path = ""
        if tmp_folder is None:
            self.tmp_folder = out_folder
        else:
            self.tmp_folder = tmp_folder
        if out_name is not None:
            self.out_name = out_name
        else:
            self.out_name = os.path.splitext(
                            os.path.basename(self.input_file))[0]
        self.out_folder = out_folder

    def index_file(self):
        self.output_file = self.tmp_folder + self.out_name
        cmd = "%skma index -i %s -o %s -Sparse" % (self.kma_folder_path,
                                                   self.input_file,
                                                   self.output_file)
        stderr = subprocess.call(cmd.split())
        if stderr != 0:
            sys.exit("Error while indexing file: %s. Cmd: %s" % (stderr, cmd))
        self.input_file = self.output_file

    def calculate_distance(self, threads, distance_method, flagOut=False,
                           matrixdisk=False):
        cmd = "%skma dist -t_db %s -o %s.phy -tmp %s -t %s -d %s" % (
              self.kma_folder_path, self.input_file,
              self.out_folder+self.out_name, self.tmp_folder, threads,
              distance_method)
        if matrixdisk:
            cmd += " -m"
        if flagOut:
            cmd += " -f 1"
        else:
            cmd += " -f 4"
        stderr = subprocess.call(cmd.split())
        if stderr != 0:
            sys.exit("Error while calcualting dist file (Error: %s). CMD: %s"
                    % (stderr, cmd))
        self.input_file = self.out_folder+self.out_name

    def cluster_data(self, construction_method, matrixdisk=False):
        self.input_file = os.path.splitext(self.input_file)[0]
        cmd = "%sccphylo tree -i %s.phy -o %s.tree -m %s -tmp %s -f 1" % (
              self.ccphylo_path, self.input_file,
              self.out_folder+self.out_name, construction_method,
              self.tmp_folder)
        if matrixdisk:
            cmd += " -mm"
        print(cmd)
        stderr = subprocess.call(cmd.split())
        if stderr != 0:
            sys.exit("Error while calcualting tree file: %" % stderr)

    @staticmethod
    def arguments():
        parser = ArgumentParser()
        group = parser.add_mutually_exclusive_group()
        group.add_argument("-ifa", "--inputfasta",
                                   dest="inputfasta",
                                   help="Input fasta file",
                                   default=None)
        group.add_argument("-idx", "--inputindexed",
                                   dest="inputindexed",
                                   help="Input KMA indexed files",
                                   default=None)
        group.add_argument("-iphy", "--inputphylip",
                                    dest="inputphylip",
                                    help="Input phylip file (end with .phy)",
                                    default=None)
        parser.add_argument("-o", "--outputPath",
                                  dest="out_path",
                                  help="Path to output folder",
                                  default='', required=True)
        parser.add_argument("-name", "--outName",
                                     dest="out_name",
                                     help="Name of the output files",
                                     default=None)
        parser.add_argument("-kp", "--kmaPath",
                                   dest="kma_path",
                                   help="Path to KMA",
                                   default=None)
        parser.add_argument("-ccp", "--ccphyloPath",
                                    dest="ccphylo_path",
                                    help="Path to ccphylo",
                                    default=None)
        parser.add_argument("-tmp", "--tmpPath",
                                    dest="tmp_path",
                                    help="Path to temporary folder",
                                    default=None)
        parser.add_argument("-f", "--flagOut",
                                  action="store_true",
                                  help="Include distance method(s) in phylip "
                                       "file")
        parser.add_argument("-nc", "--no_cluster",
                                   action="store_true",
                                   help="Create Phylip file and do not cluster"
                                        " data")
        parser.add_argument("-d", "--distMethod",
                                  dest="dist_method",
                                  help="Select the type of distance/similarity"
                                       " method(s). Types: "
                                       "{1: k-mer hamming distance,"
                                       " 2: shared k-mers,"
                                       " 4: k-mer query coverage,"
                                       " 8: k-mer template coverage,"
                                       " 16: k-mer avg. coverage,"
                                       " 32: k-mer inv. avg. coverage,"
                                       " 64: Jaccard distance,"
                                       " 128: Jaccard similarity,"
                                       " 256: Cosine distance,"
                                       " 512: Cosine similarity,"
                                       " 1024: Szymkiewicz–Simpson similarity,"
                                       " 2048: Szymkiewicz–Simpson "
                                       "dissimilarity,"
                                       " 4096: Chi-square distance}. If you "
                                       "introduce more than a method, use a "
                                       "comma for separating them.",
                                  default="1")
        parser.add_argument("-mt", "--methodTree",
                                   dest="tree_method",
                                   help="Tree construction method: "
                                   "{nj: Neighbour-Joining, "
                                   "upgma: UPGMA, "
                                   "cf: K-means Closest First, "
                                   "ff: K-means Furthest First, "
                                   "mn: Minimum Neighbours}",
                                   default="nj",
                                   choices={"nj", "upgma", "cf", "ff", "mn"})
        parser.add_argument("-m", "--matrixDisk",
                                  action="store_true",
                                  help="Allocate matrix in the disk")
        parser.add_argument("-t", "--threads",
                                  default=1,
                                  help="Number of threads used during the "
                                  "creation of the distance/similarity matrix")
        return parser.parse_args()


if __name__ == "__main__":
    args = KMA_Distance.arguments()

    if args.inputfasta is not None:
        input_file = args.inputfasta
    elif args.inputindexed is not None:
        input_file = args.inputindexed
    elif args.inputphylip is not None:
        input_file = args.inputphylip
    else:
        sys.exit("KMA distance needs a fasta, KMA indexed or phylip file as "
                 "input")
    possible_dist = ['1', '128', '4', '1024', '256', '8', '512', '16', '2',
                     '2048', '64', '32', '4096']
    dist_method_lst = args.dist_method.replace(" ", "").split(",")
    for dist in dist_method_lst:
        if dist not in possible_dist:
            sys.exit("Distance method(s) have to be among: 1, 128, 4, 1024, "
                     "256, 8, 512, 16, 2, 2048, 64, 32, 4096")
    dist_method = ", ".join(dist_method_lst)

    try_dist = KMA_Distance(input_file=input_file,
                            out_folder=args.out_path,
                            tmp_folder=args.tmp_path,
                            out_name=args.out_name,
                            kma_folder_path=args.kma_path,
                            ccphylo_path=args.ccphylo_path)
    if args.inputfasta:
        try_dist.index_file()
    if args.inputindexed or args.inputfasta:
        try_dist.calculate_distance(threads=args.threads,
                                    distance_method=dist_method,
                                    flagOut=args.flagOut,
                                    matrixdisk=args.matrixDisk)
    if args.no_cluster is False:
        try_dist.cluster_data(construction_method=args.tree_method,
                              matrixdisk=args.matrixDisk)
