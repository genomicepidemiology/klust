#!/usr/bin/env python3
from __future__ import division
import os
import treeswift
from collections import deque
from cluster import Cluster


class Tree:

    def __init__(self, file):

        self.file = file
        self.tree = self.get_tree()
        self.root = self.tree.root
        self.number_clusters = 0
        self.file_name = os.path.basename(os.path.splitext(self.file)[0])

    def get_tree(self):
        open_file = open(self.file, "r")
        char = open_file.read(1)
        while True:
            if char == "(":
                tree_string = char + open_file.readline()
                break
            char = open_file.read(1)
        tree = treeswift.read_tree_newick(tree_string)
        return tree
