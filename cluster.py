#!/usr/bin/env python3
from __future__ import division
import os
import treeswift
from collections import deque


class Cluster(list):

    CLUSTER_NUMBER = 0

    def __init__(self, out_folder=None, tree=None, leaves=None, root=None):
        if tree is not None:
            self.tree = tree.extract_subtree(root)
        else:
            self.tree = tree
        if out_folder is not None:
            self.write_cluster(out_folder=out_folder)

        Cluster.CLUSTER_NUMBER += 1

    def append(self, new_entree):
        if Cluster.__len__(self) == 0:
            self.furthest_node = new_entree
        super(Cluster, self).append(new_entree)
        self.last_node = new_entree

    def extract_leaves(self, root):
        leaves_iterator = self.tree.traverse_leaves()
        while True:
            try:
                leaf = next(leaves_iterator)
            except StopIteration:
                break
            Cluster.append(leaf)

    def write_cluster(self, out_folder):
        if not os.path.isdir(out_folder):
            raise OSError("%s is not a folder." % out_folder)
        out_file = (out_folder + "/cluster" + str(Cluster.CLUSTER_NUMBER)
                    + "_names.txt")
        with open(out_file, 'w') as outfile:
            for leaf in Cluster:
                outfile.write("%s\n" % leaf.label)
                print("Cluster, sequences:", leaf.label,
                      Cluster.CLUSTER_NUMBER)
